
const fs=require('fs');

const matchesPath="../data/matches.csv";
const deliveriesPath="../data/deliveries.csv";

const csv=require('csvtojson');

const { getYear, matchesWon, extraRunsByEachTeam, ecoBowlers ,tossAndWin, mostPOTM, strike, highestDissmised, superOver} = require('./ipl');



csv().fromFile(matchesPath).then(data=>{
    getYear(data)
    matchesWon(data)
    tossAndWin(data)
    mostPOTM(data)
    csv().fromFile(deliveriesPath).then(year=>{
        extraRunsByEachTeam(data,year)
        ecoBowlers(data,year)
        strike(data,year)
        highestDissmised(year)
       superOver(year)
        
    })

})



