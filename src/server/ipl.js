const path=require('path')

const functionPath='../public/functions'

var perYear=require(path.join(functionPath,'matchesPerYear'))
var saveOutput=require(path.join(functionPath,'saveOutput'))
const matchesWonByTeam=require(path.join(functionPath,'matchesWon'))
const extraRuns=require(path.join(functionPath,'extraRuns'))
const ecoBowler=require(path.join(functionPath,'economicalBowlers'))
const tossWin=require(path.join(functionPath,'tossAndWin'))
const hightestAward = require(path.join(functionPath,'hightestAward'))
const strikeRate = require(path.join(functionPath,'strikeRate'))
const dissmissed = require(path.join(functionPath,'dissmissed'))
const superOverEco = require(path.join(functionPath,'superOverEco'))

module.exports.getYear=(data)=>{
    saveOutput(perYear(data),"matchesPerYear.json")
}

module.exports.matchesWon=(data)=>{
    saveOutput(matchesWonByTeam(data),"matchesWon.json")
}

module.exports.extraRunsByEachTeam=(data,year)=>{
    saveOutput(extraRuns(data,year,2016),`extraRuns2016.json`)
}

module.exports.ecoBowlers=(data,year)=>{
    saveOutput(ecoBowler(data,year,2015),'economicalBowlers2015.json')
}

module.exports.tossAndWin=(data)=>{
    saveOutput(tossWin(data),"tossAndWin.json")
}

module.exports.mostPOTM=(data)=>{
    saveOutput(hightestAward(data),"mostPlayerOfTheMatch.json")
}

module.exports.strike=(data,year)=>{
    saveOutput( strikeRate(data,year),"strikeRate.json")
}

module.exports.highestDissmised=(data)=>{
    saveOutput(dissmissed(data),"highestDissmissed.json")
}

module.exports.superOver=(data)=>{
    saveOutput( superOverEco(data),"ecoSuperOver.json")
}