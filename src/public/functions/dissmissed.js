
module.exports=(data)=>{

    let res={},max=1
    for(let i of data){
        if(i.player_dismissed!==''){
            const key={
                player_dismissed:i.player_dismissed,
                bowler:i.bowler
            }
            if(res[JSON.stringify(key)]){
                res[JSON.stringify(key)]++
                if(res[JSON.stringify(key)]>max){
                    max=res[JSON.stringify(key)]
                }
            }
            else{
                res[JSON.stringify(key)]=1
                
            }
        }
    }
    const arr=[]
    for(let i in res){
        if(res[i]===max){
            arr.push({
                ...JSON.parse(i),
                times:res[i]
            }) 
        }
    }
    return arr
    
}