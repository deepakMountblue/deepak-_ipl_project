

module.exports=(data)=>{
    let res={}
    for(let i of data){
        if(i.winner==''){
            continue
        }
        if(res[i.winner]){

            if(res[i.winner][i.season]){
                res[i.winner][i.season]++
            }
            else{
                res[i.winner][i.season]=1
            }
        }
        else{
            res[i.winner]={
                [i.season]:1
            }
        }
    }
    return(res)
}