
const fs=require('fs')
const path=require('path')
module.exports= (data,fileName)=>{
    console.log(path.join(__dirname,'../output',fileName))
    const fullFilename=path.join(__dirname,'../output',fileName)
    
    fs.writeFile(fullFilename,JSON.stringify(data),(err)=>{
        if(err) console.log(err)
        else console.log("file written successfully!")
    })
    
}
