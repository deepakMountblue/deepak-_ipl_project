
module.exports=(data)=>{

    let res={}
    for(let i of data){
        if(i.is_super_over!=0){
            if(res[i.bowler]){
                res[i.bowler].runs+=parseInt(i.total_runs)
                res[i.bowler].balls++
            }
            else{
                res[i.bowler]={
                    runs:parseInt(i.total_runs),
                    balls:1
                }
            }
        }
    }
    let best={}
    for(let i in res){

        let {runs,balls}=res[i]
        let overs=parseInt(balls/6),remaining=parseInt(balls%6)
        let totalovers=parseFloat(overs)+parseFloat(remaining/6)
        let rate =parseFloat(parseFloat(runs/ totalovers).toFixed(2))
        res[i].rate=rate
        if(JSON.stringify(best)==JSON.stringify({})){
            best={
                name:i,
                rate:res[i].rate
            }
        }
        else{
            if(best['rate']>res[i].rate){
                best={
                    name:i,
                    rate:res[i].rate
                }
            }
        }
        
    }
    return best
}