module.exports=(data,yearData,year)=>{
   
    let res={},matchIdOf2016={};
    console.log(yearData[0],year)
    for(let i of data){
        if(i['season']==year){
            matchIdOf2016[i['id']]=1
        }
    }
    for(let i of yearData){
        if(matchIdOf2016[i['match_id']]){
           
            if(res[i['bowling_team']]){
                res[i['bowling_team']]+=parseInt(i['extra_runs'])
            }
            else{
                res[i['bowling_team']]=parseInt(i['extra_runs'])
                
            }
        }
    }
    return res;
}