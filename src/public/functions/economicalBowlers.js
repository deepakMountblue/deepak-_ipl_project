module.exports=(data,yearData,year)=>{
    
    let res={},matchesOfYear={}
    for(let x of data){
        if(x.season==year){
            matchesOfYear[x.id]=1;
        }
    }
    for(let x of yearData){
        if(matchesOfYear[x['match_id']]==1){
            if(res[x['bowler']]){
                res[x['bowler']]['totalruns']+=parseInt(x['total_runs']);
                res[x['bowler']]['bowls']+=1
            }
            else{
                res[x['bowler']]={
                    'totalruns':parseInt(x['total_runs']),
                    'bowls':1
                }
            }
        }
    }
    let rrr=[];

    for(x in res){
        const runs=res[x].totalruns,balls=res[x].bowls
        let overs=parseInt(balls/6),remaining=parseInt(balls%6)
        let totalovers=parseFloat(overs)+parseFloat(remaining/6)
        let rate =parseFloat(runs/ totalovers)
       rrr.push({name:x,rate});
    }
    rrr.sort((a,b)=>{
        return a.rate-b.rate;
    })

    return rrr.slice(0,10);
}