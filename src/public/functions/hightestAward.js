module.exports=(data)=>{

    let res={},ans={};
   
    for(let i of data){
        if(res[i.season]){
            if(res[i.season][i.player_of_match]){
                res[i.season][i.player_of_match]++
            }
            else{
                res[i.season][i.player_of_match]=1
            }
            
        }
        else{
            res[i.season]={
                [i.player_of_match]:1
            }
        }
        if(ans[i.season]){
            if(ans[i.season][0].won<res[i.season][i.player_of_match]){
                ans[i.season]=[{
                    name:i.player_of_match,
                    won:res[i.season][i.player_of_match]
                }]
            }
            else if(ans[i.season][0].won==res[i.season][i.player_of_match]){
                ans[i.season].push({
                    name:i.player_of_match,
                    won:res[i.season][i.player_of_match]
                })
            }
        }
        else{
            ans[i.season]=[{
                name:i.player_of_match,
                won:1
            }]
        }
    }
    return(ans)
}