module.exports=(data,year)=>{

    let seasons={},res={};
    for(let i of data){
        seasons[i.id]=i.season
    }
    for(let i of year){
        if(res[seasons[i.match_id]]){
            if(res[seasons[i.match_id]][i.batsman]){
                res[seasons[i.match_id]][i.batsman].runs+=parseInt(i.batsman_runs)
                res[seasons[i.match_id]][i.batsman].balls++
            }
            else{
                res[seasons[i.match_id]][i.batsman]={
                        runs:parseInt(i.batsman_runs),
                        balls:1
                    }   
                
            }
            
        }
        else{
            res[seasons[i.match_id]]={
                [i.batsman]:{
                    runs:parseInt(i.batsman_runs),
                    balls:1
                }   
            }
        }
    }
    let ans={}
    for(let i in res){
        for(let j in res[i]){
            
            const runs=res[i][j].runs,balls=res[i][j].balls
            let strikeRate =parseFloat((runs/ balls)*100)
            if(ans[j]){
                ans[j][i]=strikeRate
            }
            else{
                ans[j]={
                    [i]:strikeRate
                }
            }
        }
    }
    return(ans)
}