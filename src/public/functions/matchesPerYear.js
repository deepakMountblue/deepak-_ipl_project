function perYear(data){
    let res={};
    for(let x of data){
        let seasons=x.season;
        if(res[seasons]){
            res[seasons]++;
        }
        else{
            res[seasons]=1;
        }
    }
    return res;
}

module.exports=perYear